# 快速使用

## 下载源码

`git clone`到本地

## 生成脚本

确保安装Node.js后在源码根目录下执行
```
npm run build
```

## 上传脚本

将`dist/themes/default.min.css`放入“页面定制 CSS 代码”中。

将`dist/silence.min.js`填入下面html代码空白处，然后将整块代码放入“博客侧边栏公告（支持HTML代码）”中。
```html
<!--自定义侧边栏公告代码-->
<link rel="stylesheet" href="https://lib.baomitu.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!--填入生成的JS代码-->
<script type="text/javascript">


</script>
<script type="text/javascript">
    $.silence({
        favicon: 'https://files.cnblogs.com/files/kangyupl/favicon.ico',
        navigation: [
            {
                title: '主页',
                url: 'https://www.cnblogs.com/kangyupl/',
                fa_icon: 'fa-home'
            },
            {
                title: '标签',
                url: 'https://www.cnblogs.com/kangyupl/tag/',
                fa_icon: 'fa-tags'
            },
            {
                title: '归档',
                url: 'https://www.cnblogs.com/kangyupl/p/',
                fa_icon: 'fa-archive'
            },
            {
                title: '关于',
                url: 'https://www.cnblogs.com/kangyupl/articles/aboutme.html',
                fa_icon: 'fa-user'
            },
            {
                title: '友情链接',
                url: 'https://www.cnblogs.com/kangyupl/articles/friends.html',
                fa_icon: 'fa-link'
            },
            {
                title: '联系',
                url: 'https://msg.cnblogs.com/send/%E5%BA%B7%E5%AE%87PL',
                fa_icon: 'fa-envelope'
            },
            {
                title: '订阅',
                url: 'https://www.cnblogs.com/kangyupl/rss/',
                fa_icon: 'fa-rss'
            },
            {
                title: '管理',
                url: 'https://i.cnblogs.com/',
                fa_icon: 'fa-gear'
            }
        ],
        catalog: {
            enable: true,
            move: true,
            index: true,
            level1: 'h2',
            level2: 'h3',
            level3: 'h4',
        }
    });
</script>
```

## 查看效果

打开自己博客园首页即可查看。如果修改前后无变化就清空下浏览器缓存，或者换个浏览器试试。